
# My test repository


## Installation

> Before install
>
> - crate database
> - configure web server to have document root in web directory
>

Run the installation script:
```
./install.sh
```

## Populate angajati

```
php bin/console optima:angajat:demo 10
```

> Replace number 10 with other number to generate more or less entries
