#!/usr/bin/env bash

PHP=`which php`
COMPOSER=`which composer`

echo "Run composer install"
$COMPOSER install

echo "Populate database"
$PHP bin/console doctrine:schema:update --force --dump-sql

