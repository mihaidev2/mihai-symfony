<?php

namespace OptimaBundle\Command;

use Faker\Factory;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class OptimaAngajatDemoCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('optima:angajat:demo')
            ->setDescription('Generate demo angajati')
            ->addArgument('count', InputArgument::OPTIONAL, 'Argument description')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $angajatModel = $this->getContainer()->get('optima.model.angajat');

        $count = $input->getArgument('count');

        if ($count === null || (int) $count > 100) {
            $count = 5;
        }

        $faker = Factory::create();

        for ($i=1; $i<=$count; $i++) {
            $angajat = $angajatModel->create();
            $angajat->setPrenume($faker->firstName);
            $angajat->setNume($faker->lastName);
            $angajat->setEmail($faker->email);
            $angajat->setCnp($faker->ean13);
            $angajat->setBuletin($faker->creditCardNumber);
            $angajat->setTelefon($faker->phoneNumber);
            $angajat->setFunctie($faker->domainWord);

            $angajatModel->save($angajat);
        }

        $output->writeln(sprintf('<info>Am adaugat %d angajati</info>', $count));
    }
}
