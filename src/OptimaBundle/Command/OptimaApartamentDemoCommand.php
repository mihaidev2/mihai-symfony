<?php

namespace OptimaBundle\Command;

use Faker\Factory;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class OptimaApartamentDemoCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('optima:apartament:demo')
            ->setDescription('Generate demo apartamente')
            ->addArgument('count', InputArgument::OPTIONAL, 'Argument description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $apartamentModel = $this->getContainer()->get('optima.model.apartament');

        $count = $input->getArgument('count');

        if ($count === null || (int) $count > 100) {
            $count = 5;
        }

        $faker = Factory::create();

        for ($i=1; $i<=$count; $i++) {
            $apartament = $apartamentModel->create();
            $apartament->setNume($faker->lastName);
            $apartament->setCodFiscal($faker->postcode);
            $apartament->setAdresa($faker->address);
            $apartament->setReprezentant($faker->name);
            $apartament->setResponsabil($faker->name);

            $apartamentModel->save($asociatie);
        }

        $output->writeln(sprintf('<info>Am adaugat %d asociatii</info>', $count));
    }
}
