<?php

namespace OptimaBundle\Command;

use Faker\Factory;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class OptimaAsociatieDemoCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('optima:asociatie:demo')
            ->setDescription('Generate demo asociatii')
            ->addArgument('count', InputArgument::OPTIONAL, 'Argument description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $asociatieModel = $this->getContainer()->get('optima.model.asociatie');

        $count = $input->getArgument('count');

        if ($count === null || (int) $count > 100) {
            $count = 5;
        }

        $faker = Factory::create();

        for ($i=1; $i<=$count; $i++) {
            $asociatie = $asociatieModel->create();
            $asociatie->setNume($faker->lastName);
            $asociatie->setCodFiscal($faker->postcode);
            $asociatie->setAdresa($faker->address);
            $asociatie->setReprezentant($faker->name);
            $asociatie->setResponsabil($faker->name);

            $asociatieModel->save($asociatie);
        }

        $output->writeln(sprintf('<info>Am adaugat %d asociatii</info>', $count));
    }
}
