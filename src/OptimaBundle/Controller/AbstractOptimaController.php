<?php

namespace OptimaBundle\Controller;

use OptimaBundle\Entity\DatabaseEntityInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

abstract class AbstractOptimaController extends Controller
{
    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function removeAction(Request $request)
    {
        $id = $request->request->getInt('id'); // $_POST['id']

        $model = $this->getControllerModel();

        $entity = $this->getEntityByIdForDelete($id);

        $model->remove($entity);

        $response = [
            'code' => 200,
        ];

        return new JsonResponse($response);
    }

    /**
     * @param int $id
     *
     * @return DatabaseEntityInterface
     */
    protected function getEntityByIdForDelete($id)
    {
        $model = $this->getControllerModel();

        return $model->find($id);
    }

    /**
     * @param int $id
     *
     * @return string
     */
    protected function getEditPageTitle($id, $section)
    {
        $prefix = 'Adauga';
        if ($id > 0) {
            $prefix = 'Modifica';
        }

        return $prefix . ' ' . $section;
    }

    /**
     * @return object
     */
    abstract protected function getControllerModel();
}
