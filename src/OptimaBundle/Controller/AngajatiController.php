<?php

namespace OptimaBundle\Controller;

use OptimaBundle\Forms\AngajatForm;
use Symfony\Component\HttpFoundation\Request;

class AngajatiController extends AbstractOptimaController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $users = $this->getControllerModel()->getList();

        return $this->render(
            'OptimaBundle:Angajati:index.html.twig',
            [
                'users' => $users,
            ]
        );
    }

    /**
     * @param Request $request
     * @param int $id
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, $id = 0)
    {
        $model = $this->getControllerModel();

        $default = $model->getAngajatOrNew($id);

        $form = $this->createForm(AngajatForm::class, $default);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();

            $model->save($data);

            return $this->redirect(
                $this->generateUrl('optima_angajati')
            );
        }

        return $this->render(
            'OptimaBundle::edit.html.twig',
            [
                'form' => $form->createView(),
                'page_title' => $this->getEditPageTitle($id, 'angajat'),
            ]
        );
    }

    /**
     * @return object|\OptimaBundle\Services\AngajatModelService
     */
    protected function getControllerModel()
    {
        return $this->get('optima.model.angajat');
    }
}
