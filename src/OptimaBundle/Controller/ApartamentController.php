<?php

namespace OptimaBundle\Controller;

use OptimaBundle\Forms\ApartamentForm;
use Symfony\Component\HttpFoundation\Request;

class ApartamentController extends AbstractOptimaController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $apartamente = $this->getControllerModel()->getList();

        return $this->render('OptimaBundle:Apartament:index.html.twig', [
            'apartamente' => $apartamente,
        ]);
    }

    /**
     * @param Request $request
     * @param int $id
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, $id = 0)
    {
        $model = $this->getControllerModel();

        $defaultData = $model->getByIdOrCreate($id);

        $form = $this->createForm(ApartamentForm::class, $defaultData);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $formData = $form->getData();

            $model->save($formData);

            return $this->redirect(
               $this->generateUrl('optima_apartamente_edit', [
                   'id' => $formData->getId(),
               ])
           );
        }

        return $this->render('OptimaBundle::edit.html.twig', [
            'form' => $form->createView(),
            'page_title' => $this->getEditPageTitle($id, 'apartament'),
        ]);
    }

    /**
     * @return object|\OptimaBundle\Services\ApartamentModelService
     */
    protected function getControllerModel()
    {
        return $this->get('optima.model.apartament');
    }
}
