<?php

namespace OptimaBundle\Controller;

use OptimaBundle\Forms\AsociatieForm;
use Symfony\Component\HttpFoundation\Request;

class AsociatieController extends AbstractOptimaController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $asociatii = $this->getControllerModel()->getList();

        return $this->render('OptimaBundle:Asociatie:index.html.twig', [
            'asociatii' => $asociatii,
        ]);
    }

    /**
     * @param Request $request
     * @param int $id
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, $id = 0)
    {
        $model = $this->getControllerModel();

        $defaultData = $model->getByIdOrCreate($id);

        $form = $this->createForm(AsociatieForm::class, $defaultData);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $formData = $form->getData();

            $model->save($formData);

            return $this->redirect(
                $this->generateUrl('optima_asociatii_edit', [
                    'id' => $formData->getId(),
                ])
            );
        }

        return $this->render('OptimaBundle::edit.html.twig', [
            'form' => $form->createView(),
            'page_title' => $this->getEditPageTitle($id, 'asociatie'),
        ]);
    }

    /**
     * @return object|\OptimaBundle\Services\AsociatieModelService
     */
    protected function getControllerModel()
    {
        return $this->get('optima.model.asociatie');
    }
}
