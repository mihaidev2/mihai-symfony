<?php

namespace OptimaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('OptimaBundle:Default:index.html.twig');
    }
}
