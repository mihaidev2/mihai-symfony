<?php

namespace OptimaBundle\Controller;

use OptimaBundle\Entity\DatabaseEntityInterface;
use OptimaBundle\Forms\LocatarForm;
use Symfony\Component\HttpFoundation\Request;

class LocatarController extends AbstractOptimaController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $locatari = $this->getControllerModel()->getList();

        return $this->render('OptimaBundle:Locatar:index.html.twig', [
            'locatari' => $locatari,
        ]);
    }

    /**
     * @param Request $request
     * @param int $id
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, $id = 0)
    {
        $model = $this->getControllerModel();

        $defaultData = $model->getByIdOrCreate($id);

        $form = $this->createForm(LocatarForm::class, $defaultData);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $formData = $form->getData();

            $model->save($formData);

            return $this->redirect(
                $this->generateUrl('optima_locatari_edit', [
                    'id' => $formData->getId(),
                ])
            );
        }

        return $this->render('OptimaBundle::edit.html.twig', [
            'form' => $form->createView(),
            'page_title' => $this->getEditPageTitle($id, 'locatar'),
        ]);
    }

    /**
     * @param int $id
     *
     * @return DatabaseEntityInterface
     */
    protected function getEntityByIdForDelete($id)
    {
        $model = $this->getControllerModel();

        /** @var DatabaseEntityInterface $entity */
        $entity = $model->find($id);
        $newEmail = $entity->getEmail() . '-deleted-' . time();
        $entity->setEmail($newEmail);

        return $entity;
    }

    /**
     * @return \OptimaBundle\Services\LocatarModelService
     */
    protected function getControllerModel()
    {
        return $this->get('optima.model.locatar');
    }
}
