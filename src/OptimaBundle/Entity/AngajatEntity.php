<?php

namespace OptimaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="angajati", options={"collate": "utf8_general_ci", "charset": "utf8"})
 * @ORM\Entity(repositoryClass="OptimaBundle\Repository\AngajatRepository")
 * @UniqueEntity("email")
 */
class AngajatEntity implements DatabaseEntityInterface
{
    /**
     * @ORM\Column(type="integer", options={"unsigned": true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"default": 1})
     */
    private $status = DatabaseEntityInterface::STATUS_INACTIVE;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nume;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $prenume;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cnp;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $buletin;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true, options={"unique": true})
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $telefon;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $functie;

    /**
     * @return string
     */
    public function getFullName()
    {
        return $this->getPrenume() . ' ' . $this->getNume();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getFullName() . ' | ' . $this->getEmail();
    }

    /**
     * @return bool
     */
    public function isDeleted()
    {
        return $this->getStatus() === DatabaseEntityInterface::STATUS_DELETED;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNume()
    {
        return $this->nume;
    }

    /**
     * @param string $nume
     */
    public function setNume($nume)
    {
        $this->nume = $nume;
    }

    /**
     * @return string
     */
    public function getPrenume()
    {
        return $this->prenume;
    }

    /**
     * @param string $prenume
     */
    public function setPrenume($prenume)
    {
        $this->prenume = $prenume;
    }

    /**
     * @return string
     */
    public function getCnp()
    {
        return $this->cnp;
    }

    /**
     * @param string $cnp
     */
    public function setCnp($cnp)
    {
        $this->cnp = $cnp;
    }

    /**
     * @return string
     */
    public function getBuletin()
    {
        return $this->buletin;
    }

    /**
     * @param string $buletin
     */
    public function setBuletin($buletin)
    {
        $this->buletin = $buletin;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getTelefon()
    {
        return $this->telefon;
    }

    /**
     * @param string $telefon
     */
    public function setTelefon($telefon)
    {
        $this->telefon = $telefon;
    }

    /**
     * @return string
     */
    public function getFunctie()
    {
        return $this->functie;
    }

    /**
     * @param string $functie
     */
    public function setFunctie($functie)
    {
        $this->functie = $functie;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }
}
