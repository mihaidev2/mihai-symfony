<?php

namespace OptimaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="apartamente", options={"collate": "utf8_general_ci", "charset": "utf8"})
 * @ORM\Entity(repositoryClass="OptimaBundle\Repository\ApartamentRepository")
 */
class ApartamentEntity implements DatabaseEntityInterface
{
    /**
     * @ORM\Column(type="integer", options={"unsigned": true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"default": 1})
     */
    private $status = DatabaseEntityInterface::STATUS_INACTIVE;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $numar;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=8)
     */
    private $bloc;

    /**
     * @var int
     *
     * @ORM\Column(type="string", length=5)
     */
    private $scara;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", length=4)
     */
    private $suprafata;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getNumar()
    {
        return $this->numar;
    }

    /**
     * @param int $numar
     */
    public function setNumar($numar)
    {
        $this->numar = $numar;
    }

    /**
     * @return mixed
     */
    public function getBloc()
    {
        return $this->bloc;
    }

    /**
     * @param mixed $bloc
     */
    public function setBloc($bloc)
    {
        $this->bloc = $bloc;
    }

    /**
     * @return int
     */
    public function getScara()
    {
        return $this->scara;
    }

    /**
     * @param int $scara
     */
    public function setScara($scara)
    {
        $this->scara = $scara;
    }

    /**
     * @return int
     */
    public function getSuprafata()
    {
        return $this->suprafata;
    }

    /**
     * @param int $suprafata
     */
    public function setSuprafata($suprafata)
    {
        $this->suprafata = $suprafata;
    }
}
