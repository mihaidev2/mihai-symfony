<?php

namespace OptimaBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="asociatii", options={"collate": "utf8_general_ci", "charset": "utf8"})
 * @ORM\Entity(repositoryClass="OptimaBundle\Repository\AsociatieRepository")
 */
class AsociatieEntity implements DatabaseEntityInterface
{
    /**
     * @ORM\Column(type="integer", options={"unsigned": true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true, options={"unsigned": true})
     */
    private $id_angajat;

    /**
     * @var AngajatEntity
     *
     * @ORM\ManyToOne(targetEntity="AngajatEntity")
     * @ORM\JoinColumn(name="id_angajat", referencedColumnName="id")
     */
    private $angajat;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"default": 1})
     */
    private $status = DatabaseEntityInterface::STATUS_INACTIVE;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $nume;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $cod_fiscal;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $adresa;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $reprezentant;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $responsabil;

    public function __construct()
    {
        $this->angajat = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getIdAngajat()
    {
        return $this->id_angajat;
    }

    /**
     * @param int $id_angajat
     */
    public function setIdAngajat($id_angajat)
    {
        $this->id_angajat = $id_angajat;
    }

    /**
     * @return AngajatEntity
     */
    public function getAngajat()
    {
        return $this->angajat;
    }

    /**
     * @param AngajatEntity $angajat
     */
    public function setAngajat($angajat)
    {
        $this->angajat = $angajat;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getNume()
    {
        return $this->nume;
    }

    /**
     * @param string $nume
     */
    public function setNume($nume)
    {
        $this->nume = $nume;
    }

    /**
     * @return string
     */
    public function getCodFiscal()
    {
        return $this->cod_fiscal;
    }

    /**
     * @param string $cod_fiscal
     */
    public function setCodFiscal($cod_fiscal)
    {
        $this->cod_fiscal = $cod_fiscal;
    }

    /**
     * @return string
     */
    public function getAdresa()
    {
        return $this->adresa;
    }

    /**
     * @param string $adresa
     */
    public function setAdresa($adresa)
    {
        $this->adresa = $adresa;
    }

    /**
     * @return string
     */
    public function getReprezentant()
    {
        return $this->reprezentant;
    }

    /**
     * @param string $reprezentant
     */
    public function setReprezentant($reprezentant)
    {
        $this->reprezentant = $reprezentant;
    }

    /**
     * @return string
     */
    public function getResponsabil()
    {
        return $this->responsabil;
    }

    /**
     * @param string $responsabil
     */
    public function setResponsabil($responsabil)
    {
        $this->responsabil = $responsabil;
    }

    /**
     * @return int
     */
    public function getIdResponsabil()
    {
        return $this->id_responsabil;
    }

    /**
     * @param int $id_responsabil
     */
    public function setIdResponsabil($id_responsabil)
    {
        $this->id_responsabil = $id_responsabil;
    }
}
