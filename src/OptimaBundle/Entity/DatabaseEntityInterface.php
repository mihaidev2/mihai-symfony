<?php

namespace OptimaBundle\Entity;

interface DatabaseEntityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_INACTIVE = 1;
    const STATUS_ACTIVE = 2;

    /**
     * @param int $status
     *
     * @return void
     */
    public function setStatus($status);
}
