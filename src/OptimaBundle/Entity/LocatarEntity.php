<?php

namespace OptimaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="locatari", options={"collate": "utf8_general_ci", "charset": "utf8"})
 * @ORM\Entity(repositoryClass="OptimaBundle\Repository\LocatarRepository")
 */
class LocatarEntity implements DatabaseEntityInterface
{
    /**
     * @ORM\Column(type="integer", options={"unsigned": true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"default": 1})
     */
    private $status = DatabaseEntityInterface::STATUS_INACTIVE;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $prenume;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $nume;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, options={"unique": true})
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $telefon;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getPrenume()
    {
        return $this->prenume;
    }

    /**
     * @param string $prenume
     */
    public function setPrenume($prenume)
    {
        $this->prenume = $prenume;
    }

    /**
     * @return string
     */
    public function getNume()
    {
        return $this->nume;
    }

    /**
     * @param string $nume
     */
    public function setNume($nume)
    {
        $this->nume = $nume;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getTelefon()
    {
        return $this->telefon;
    }

    /**
     * @param string $telefon
     */
    public function setTelefon($telefon)
    {
        $this->telefon = $telefon;
    }
}
