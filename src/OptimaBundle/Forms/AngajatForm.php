<?php

namespace OptimaBundle\Forms;

use OptimaBundle\Entity\AngajatEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class AngajatForm extends AbstractType
{
    /**
     * @param OptionsResolver $optionsResolver
     */
    public function configureOptions(OptionsResolver $optionsResolver)
    {
        $optionsResolver->setDefaults([
            'attr' => [
                'novalidate' => 'novalidate',
            ],
            'data_class' => AngajatEntity::class,
        ]);
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nume', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('prenume')
            ->add('cnp', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('buletin')
            ->add('email', EmailType::class, [
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('telefon')
            ->add('functie')
        ;
    }
}
