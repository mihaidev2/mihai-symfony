<?php

namespace OptimaBundle\Forms;

use OptimaBundle\Entity\ApartamentEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class ApartamentForm extends AbstractType
{
    /**
     * @param OptionsResolver $optionsResolver
     */
    public function configureOptions(OptionsResolver $optionsResolver)
    {
        $optionsResolver->setDefaults([
            'attr' => [
                'novalidate' => 'novalidate',
            ],
            'data_class' => ApartamentEntity::class,
        ]);
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('numar', IntegerType::class, [
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('bloc', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('scara', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('suprafata', IntegerType::class, [
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('status', ChoiceType::class, [
                'expanded' => true,
                'choices' => [
                    'Active' => ApartamentEntity::STATUS_ACTIVE,
                    'Inactive' => ApartamentEntity::STATUS_INACTIVE,
                ],
            ])
        ;
    }
}
