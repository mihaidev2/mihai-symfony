<?php

namespace OptimaBundle\Forms;

use OptimaBundle\Entity\AsociatieEntity;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class AsociatieForm extends AbstractType
{
    /**
     * @param OptionsResolver $optionsResolver
     */
    public function configureOptions(OptionsResolver $optionsResolver)
    {
        $optionsResolver->setDefaults([
            'attr' => [
                'novalidate' => 'novalidate',
            ],
            'data_class' => AsociatieEntity::class,
        ]);
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nume', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('status', ChoiceType::class, [
                'expanded' => true,
                'choices' => [
                    'Active' => AsociatieEntity::STATUS_ACTIVE,
                    'Inactive' => AsociatieEntity::STATUS_INACTIVE,
                ],
            ])
            ->add('cod_fiscal')
            ->add('adresa', TextareaType::class)
            ->add('reprezentant')
        ;

        $builder->add('angajat', EntityType::class, [
            // query choices from this entity
            'class' => 'OptimaBundle\Entity\AngajatEntity',

            // use the User.username property as the visible option string
            'choice_label' => 'full_name',
        ]);
    }
}
