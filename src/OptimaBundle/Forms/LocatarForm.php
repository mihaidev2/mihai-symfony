<?php

namespace OptimaBundle\Forms;

use OptimaBundle\Entity\LocatarEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class LocatarForm extends AbstractType
{
    /**
     * @param OptionsResolver $optionsResolver
     */
    public function configureOptions(OptionsResolver $optionsResolver)
    {
        $optionsResolver->setDefaults([
            'attr' => [
                'novalidate' => 'novalidate',
            ],
            'data_class' => LocatarEntity::class,
        ]);
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('prenume', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('status', ChoiceType::class, [
                'expanded' => true,
                'choices' => [
                    'Active' => LocatarEntity::STATUS_ACTIVE,
                    'Inactive' => LocatarEntity::STATUS_INACTIVE,
                ],
            ])
            ->add('nume', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('email', EmailType::class, [
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('telefon', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                ],
            ])
        ;
    }
}
