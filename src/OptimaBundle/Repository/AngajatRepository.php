<?php

namespace OptimaBundle\Repository;

use Doctrine\ORM\EntityRepository;
use OptimaBundle\Entity\AngajatEntity;

class AngajatRepository extends EntityRepository
{
    /**
     * @return AngajatEntity[]
     */
    public function getList()
    {
        $query = $this->createQueryBuilder('a')
            ->where('a.status > 0')
            ->orderby('a.id', 'DESC')
            ->getQuery()
        ;

        return $query->getResult();
    }
}
