<?php

namespace OptimaBundle\Repository;

use Doctrine\ORM\EntityRepository;

class ApartamentRepository extends EntityRepository
{
    public function getList()
    {
        $query = $this->createQueryBuilder('a')
            ->where('a.status > 0')
            ->orderby('a.id', 'DESC')
            ->getQuery()
        ;

        return $query->getResult();
    }
}
