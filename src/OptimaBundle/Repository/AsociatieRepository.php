<?php

namespace OptimaBundle\Repository;

use Doctrine\ORM\EntityRepository;

class AsociatieRepository extends EntityRepository
{
    public function getList()
    {
        $query = $this->createQueryBuilder('a')
            ->where('a.status > 0')
            ->orderBy('a.id', 'DESC')
            ->getQuery()
        ;

        return $query->getResult();
    }
}
