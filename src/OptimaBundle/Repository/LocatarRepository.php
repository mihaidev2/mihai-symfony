<?php

namespace OptimaBundle\Repository;

use Doctrine\ORM\EntityRepository;
use OptimaBundle\Entity\LocatarEntity;

class LocatarRepository extends EntityRepository
{
    /**
     * @return LocatarEntity[]
     */
    public function getList()
    {
        $query = $this->createQueryBuilder('a')
            ->where('a.status > 0')
            ->orderBy('a.id', 'DESC')
            ->getQuery()
        ;

        return $query->getResult();
    }
}
