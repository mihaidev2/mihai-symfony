'use strict';

function AjaxRequest(element){
    if ( ! confirm('Actiune de stergere. Continui ?')) {
        return;
    }

    this.objectId = element.data('object-id');

    this.makeAjaxRequest();
}

AjaxRequest.prototype.makeAjaxRequest = function(){
    var self = this;
    $.ajax({
        url: ajax_url,
        type: 'POST',
        dataType: 'json',
        data: {
            id: this.objectId
        }
    }).done(function(ajaxResponse){
        return self.renderResponse(ajaxResponse);
    });
};

AjaxRequest.prototype.renderResponse = function(ajaxResponse){
    if (ajaxResponse.code === 200) {
        $('#row-' + this.objectId)
            .css({background: 'red', color: 'white'})
            .fadeOut('slow')
        ;
        return;
    }

    alert(ajaxResponse.message);
};


$(document).ready(function(){
    $('.remove-entity').on('click', function(event){
        event.preventDefault();

        new AjaxRequest($(this));
    });
});
