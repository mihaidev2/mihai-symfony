<?php

namespace OptimaBundle\Services;

use Doctrine\ORM\EntityManagerInterface;
use OptimaBundle\Entity\DatabaseEntityInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

abstract class AbstractModelService
{
    /** Rewrite this constant in your model */
    const REPOSITORY = '';

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        if (empty(static::REPOSITORY)) {
            $message = sprintf('You must declare REPOSITORY constant in %s', get_called_class());
            throw new NotFoundHttpException($message);
        }
        $this->em = $em;
    }

    /**
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    protected function getRepository()
    {
        return $this->em->getRepository(static::REPOSITORY);
    }

    /**
     * @param $entity
     *
     * @return mixed
     */
    public function save($entity)
    {
        $this->em->persist($entity);
        $this->em->flush();

        return $entity;
    }
    /**
     * @param int $id
     *
     * @return null|object
     */
    public function find($id)
    {
        return $this
            ->getRepository()
            ->find($id)
        ;
    }
    /**
     * @param array $criteria
     *
     * @return null|object
     */
    public function findOneBy(array $criteria)
    {
        return $this->getRepository()
            ->findOneBy($criteria)
        ;
    }
    /**
     * @param array $criteria
     * @param array|null $orderBy
     * @param null $limit
     * @param null $offset
     *
     * @return array
     */
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {
        return $this->getRepository()
            ->findBy($criteria, $orderBy, $limit, $offset)
        ;
    }
    /**
     * @return array
     */
    public function findAll()
    {
        return $this
            ->getRepository()
            ->findAll()
        ;
    }

    /**
     * @param DatabaseEntityInterface $entity
     *
     * @return bool
     */
    public function remove(DatabaseEntityInterface $entity)
    {
        $entity->setStatus(DatabaseEntityInterface::STATUS_DELETED);

        $this->save($entity);

        return true;
    }
}
