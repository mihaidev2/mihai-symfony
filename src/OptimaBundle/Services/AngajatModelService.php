<?php

namespace OptimaBundle\Services;

use OptimaBundle\Entity\AngajatEntity;
use OptimaBundle\Repository\AngajatRepository;

/**
 * @method AngajatRepository getRepository()
 */
class AngajatModelService extends AbstractModelService
{
    const REPOSITORY = 'OptimaBundle:AngajatEntity';

    /**
     * @return AngajatEntity[]
     */
    public function getList()
    {
        return $this->getRepository()->getList();
    }

    /**
     * @param int $id
     *
     * @return AngajatEntity
     */
    public function getAngajatOrNew($id = 0)
    {
        $angajat = $this->find($id);

        if ($angajat === null) {
            $angajat = $this->create();
        }

        return $angajat;
    }

    /**
     * @return AngajatEntity
     */
    public function create()
    {
        return new AngajatEntity();
    }
}
