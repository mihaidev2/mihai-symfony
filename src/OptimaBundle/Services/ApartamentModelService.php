<?php

namespace OptimaBundle\Services;

use OptimaBundle\Entity\ApartamentEntity;

/**
 * @method ApartamentRepository getRepository()
 */
class ApartamentModelService extends AbstractModelService
{
    const REPOSITORY = 'OptimaBundle:ApartamentEntity';

    /**
     * @return ApartamentEntity[]
     */
    public function getList()
    {
        return $this->getRepository()->getList();
    }

    /**
     * @param int $id
     *
     * @return ApartamentEntity
     */
    public function getByIdOrCreate($id)
    {
        $apartament = $this->getRepository()->find($id);

        if ($apartament === null) {
            $apartament = new ApartamentEntity();
        }

        return $apartament;
    }
}
