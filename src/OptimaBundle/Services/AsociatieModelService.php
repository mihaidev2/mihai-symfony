<?php

namespace OptimaBundle\Services;

use OptimaBundle\Entity\AsociatieEntity;
use OptimaBundle\Repository\AsociatieRepository;

/**
 * @method AsociatieRepository getRepository()
 */
class AsociatieModelService extends AbstractModelService
{
    const REPOSITORY = 'OptimaBundle:AsociatieEntity';

    /**
     * @return AsociatieEntity[]
     */
    public function getList()
    {
        return $this->getRepository()->getList();
    }

    /**
     * @param int $id
     *
     * @return AsociatieEntity
     */
    public function getByIdOrCreate($id)
    {
        $asociatie = $this->getRepository()->find($id);

        if ($asociatie === null) {
            $asociatie = new AsociatieEntity();
        }

        return $asociatie;
    }
}
