<?php

namespace OptimaBundle\Services;

use OptimaBundle\Entity\LocatarEntity;
use OptimaBundle\Repository\LocatarRepository;

/**
 * @method LocatarRepository getRepository()
 */
class LocatarModelService extends AbstractModelService
{
    const REPOSITORY = 'OptimaBundle:LocatarEntity';

    /**
     * @return \OptimaBundle\Entity\LocatarEntity[]
     */
    public function getList()
    {
        return $this->getRepository()->getList();
    }

    /**
     * @param int $id
     *
     * @return LocatarEntity
     */
    public function getByIdOrCreate($id)
    {
        $locatar = $this->getRepository()->find($id);

        if ($locatar === null) {
            $locatar = new LocatarEntity();
        }

        return $locatar;
    }
}
