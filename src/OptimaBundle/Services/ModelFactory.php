<?php

namespace OptimaBundle\Services;

use Doctrine\ORM\EntityManagerInterface;

class ModelFactory
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * ModelFactory constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @return LocatarModelService
     */
    public function createLocatarModel()
    {
        return new LocatarModelService($this->em);
    }
}
